package org.fhkiel.utilities

class Properties{  
    static final String FILE_NAME ='fileName'
    static final String FILE_NOT_FOUND_MESSAGE ='File not found'
    static final String ENVIRONMENT_CONFIGURATIONS_PATH ='org/fhkiel/EnvironmentConfigurations.yaml'
    static final String STRATEGY_CONFIGURATIONS_PATH ='org/fhkiel/StrategyConfigurations.yaml'
    static final String PROJECT_ENVIRONMENT_CONFIGURATIONS_PATH ='/config/EnvironmentConfigurations.yaml'
    static final String PROJECT_STRATEGY_CONFIGURATIONS_PATH ='/config/StrategyConfigurations.yaml'
}
