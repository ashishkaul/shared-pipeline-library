package org.fhkiel.usecase

import org.fhkiel.entities.ConfigurationsUpdater

class UpdateConfigurations{
    static Map<String, String> update(Map<String, String> targetYamlMap, Map<String, String> sourceYamlMap){
        return ConfigurationsUpdater.update(targetYamlMap, sourceYamlMap)
    }
}
