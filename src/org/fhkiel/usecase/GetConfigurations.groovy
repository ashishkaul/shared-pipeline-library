package org.fhkiel.usecase

@Grab('org.yaml:snakeyaml:1.17')
import org.yaml.snakeyaml.Yaml
import org.fhkiel.entities.ConfigurationsProvider

class GetConfigurations{
    static Map<String, String> get(def yamlConfig){
        Yaml yamlParser = new Yaml()
        return ConfigurationsProvider.get(yamlParser, yamlConfig)
    }
}