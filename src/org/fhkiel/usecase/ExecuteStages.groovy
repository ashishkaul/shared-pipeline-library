package org.fhkiel.usecase

import org.fhkiel.entities.*

class ExecuteStages{
    static void buildSourceCode(Map<String, String> environmentConfigurations, Script script, String workspace){
        BuildExecutor.build(environmentConfigurations, script, workspace)
    }
    static void executeUnitTests(Map<String, String> environmentConfigurations, Script script){
        UnitTestExecutor.execute(environmentConfigurations, script)
    }
    static void runStaticCodeAnalysis(Map<String, String> environmentConfigurations, Script script){
        StaticAnalysisExecutor.analyze(environmentConfigurations, script)
    }
    static void createArtifact(Map<String, String> environmentConfigurations, Script script){
        ArtifactCreator.create(environmentConfigurations, script)
    }
    static void storeArtifact(Map<String, String> environmentConfigurations, Script script){
        ArtifactStorageExecutor.store(environmentConfigurations, script)
    }
    static void deployArtifact(Map<String, String> environmentConfigurations, Map<String, String> strategyConfigurations, Script script){
        DeploymentExecutor.deploy(environmentConfigurations, strategyConfigurations, script)
    }
    static void executeOnDeviceTests(Map<String, String> environmentConfigurations, Map<String, String> strategyConfigurations, Script script){
        OnDeviceTestExecutor.execute(environmentConfigurations, strategyConfigurations, script)
    }
}