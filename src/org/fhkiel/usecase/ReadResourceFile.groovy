package org.fhkiel.usecase

import org.fhkiel.entities.ResourceFileReader

class ReadResourceFile{
    static String read(String path, Script script){
        if(script.fileExists(path)){
            return ResourceFileReader.getYaml(path, script)
        }else{
            return "File not found"
        }
    }
}
