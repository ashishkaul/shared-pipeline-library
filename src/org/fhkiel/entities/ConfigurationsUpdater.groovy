package org.fhkiel.entities

class ConfigurationsUpdater{
    static Map<String,String> update(Map targetYamlMap, Map sourceYamlMap){
        for(item in sourceYamlMap){
            if(targetYamlMap.containsKey(item.key)){
                targetYamlMap.put(item.key, item.value)
            }
        }
        return targetYamlMap
    }
}
