package org.fhkiel.entities

class ConfigurationsProvider{
    static Map<String, String> get(def yamlParser, def yamlConfig){
        Map result = yamlParser.load(yamlConfig)
        return flattenMap(result)
    }

    static String createKey(prefix, key){
        (prefix?.length() > 0) ? "${prefix}.${key}" : key
    }

    static Map flattenMap(LinkedHashMap aMap, prefix=null){
        aMap.inject([:]) {map, entry ->
            if(entry.value instanceof LinkedHashMap){
                map += flattenMap(entry.value, createKey(prefix, entry.key))
            } else{
                map."${createKey(prefix, entry.key)}" = entry.value
            }
            map
        }
    }

    // static Map<String, String> ReplaceConfigurations(Map targetYamlMap, Map sourceYamlMap){
    //     for(item in sourceYamlMap){
    //         if(targetYamlMap.containsKey(item.key)){
    //             targetYamlMap.put(item.key, item.value)
    //         }
    //     }
    //     return targetYamlMap
    // }
}
