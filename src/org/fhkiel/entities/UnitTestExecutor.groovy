package org.fhkiel.entities

class UnitTestExecutor{
    static void execute(Map<String, String> environmentConfigurations, Script script){

        script.withEnv(['PATH+PIO=/usr/local/bin/']){
            script.withCredentials([script.string(credentialsId: 'amazon', variable: 'token')]){
                script.sh("set +x")
                script.sh '''
                PLATFORMIO_AUTH_TOKEN=token
                '''
                script.sh(environmentConfigurations["stages.unit-test.goals"])
            }
        }
    }
}