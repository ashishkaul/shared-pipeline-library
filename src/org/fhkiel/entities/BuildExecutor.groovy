package org.fhkiel.entities

class BuildExecutor{
    static void build(Map<String, String> environmentConfigurations, Script script, String workspace){
        script.withEnv(['PATH+PIO=/usr/local/bin/']){
            script.withCredentials([script.string(credentialsId: 'amazon', variable: 'token')]){
                script.sh("set +x")
                script.sh '''
                PLATFORMIO_AUTH_TOKEN=token
                '''
                script.sh("${environmentConfigurations['stages.build.goals']}")
            }
        }
    }
}
