package org.fhkiel.entities

class OnDeviceTestExecutor{
    static void execute(Map<String, String> environmentConfigurations,Map<String, String> strategyConfigurations, Script script){
        script.withEnv(['PATH+PIO=/usr/local/bin/']){
            script.withCredentials([script.string(credentialsId: 'amazon', variable: 'token')]){
                script.sh("set +x")
                script.sh '''
                PLATFORMIO_AUTH_TOKEN=token
                '''
                script.sh("${environmentConfigurations['stages.on-device-test.goals']}")
            }
        }
    }
}
