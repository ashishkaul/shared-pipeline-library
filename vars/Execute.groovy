import org.fhkiel.utilities.Properties
import org.fhkiel.usecase.*
// import org.fhkiel.usecase.ReadResourceFile
// import org.fhkiel.usecase.GetConfigurations
// import org.fhkiel.usecase.UpdateConfigurations

def call(){
    def defaultEnvironmentConfigurations = null
    def defaultStrategyConfigurations = null
    def projectEnvironmentConfigurations = null
    def projectStrategyConfigurations = null
    def environmentConfigurations = null
    def strategyConfigurations = null
    pipeline{
        stage('Setup Configurations'){
            try{
                defaultEvironmentConfigYaml = this.libraryResource(Properties.ENVIRONMENT_CONFIGURATIONS_PATH)
                defaultSrategyConfigYaml = this.libraryResource(Properties.STRATEGY_CONFIGURATIONS_PATH)
                defaultEnvironmentConfigurations = GetConfigurations.get(defaultEvironmentConfigYaml)
                defaultStrategyConfigurations = GetConfigurations.get(defaultSrategyConfigYaml)

                // this.sh("echo ${defaultEnvironmentConfigurations}")
            }catch(Exception ex){
                throw ex
            }   
        }
        stage('Checkout SCM'){
            try{
                checkout scm
            }catch(Exception ex){
                throw ex
            }
        }
        stage('Read Configurations'){
            try{
                
                projectEnvironmentConfigYaml = ReadResourceFile.read("${this.WORKSPACE}${Properties.PROJECT_ENVIRONMENT_CONFIGURATIONS_PATH}", this)
                projectStrategyConfigYaml = ReadResourceFile.read("${this.WORKSPACE}${Properties.PROJECT_STRATEGY_CONFIGURATIONS_PATH}", this)

                if(projectEnvironmentConfigYaml != Properties.FILE_NOT_FOUND_MESSAGE){
                    projectEnvironmentConfigurations = GetConfigurations.get(projectEnvironmentConfigYaml)
                }
                if(projectStrategyConfigYaml != Properties.FILE_NOT_FOUND_MESSAGE){
                    projectStrategyConfigurations = GetConfigurations.get(projectStrategyConfigYaml)
                }
                

                
            }catch(Exception ex){
                throw ex
            }
        }
        stage('Update Configurations'){
            try{
                environmentConfigurations = UpdateConfigurations.update(defaultEnvironmentConfigurations, projectEnvironmentConfigurations)
                strategyConfigurations = UpdateConfigurations.update(defaultStrategyConfigurations, projectStrategyConfigurations)
            }catch(Exception ex){
                throw ex
            }
        }
        // if(environmentConfigurations['configurations.execution.skip-build'] == false){
            stage('Compile'){
                // try{
                    sh('echo compile')
                    ExecuteStages.buildSourceCode(environmentConfigurations, this, this.WORKSPACE)

                // }catch(Exception ex){
                    // throw ex
                // }
            }
        // }

        if(environmentConfigurations['configurations.execution.skip-static-analysis'] == false){
            stage('Static Analysis'){
                try{
                    ExecuteStages.runStaticCodeAnalysis(environmentConfigurations, this)

                }catch(Exception ex){
                    throw ex
                }
            }
            stage('Quality Gate'){
                try{

                }catch(Exception ex){
                    throw ex
                }
            }
        }

        if(environmentConfigurations['configurations.execution.skip-unit-test'] == false){
            stage('Test'){
                try{
                    ExecuteStages.executeUnitTests(environmentConfigurations, this)

                }catch(Exception ex){
                    throw ex
                }
            }
        }
        
        if(environmentConfigurations['configurations.execution.skip-package'] == false){
            stage('Create Artifact'){
                try{
                    ExecuteStages.createArtifact(environmentConfigurations, this)

                }catch(Exception ex){
                    throw ex
                }
            }
        }

        if(environmentConfigurations['configurations.execution.skip-artifact-storage'] == false){
            stage('Artifact Storage'){
                try{
                    ExecuteStages.storeArtifact(environmentConfigurations, this)

                }catch(Exception ex){
                    throw ex
                }
            }
        }

        if(environmentConfigurations['configurations.execution.skip-deploy-artifact'] == false){
            stage('On Device Test'){
                try{
                    ExecuteStages.executeOnDeviceTests(environmentConfigurations, environmentConfigurations, this)

                }catch(Exception ex){
                    throw ex
                }
            }
        }

        if(environmentConfigurations['configurations.execution.skip-deploy-artifact'] == false){
            stage('Deploy'){
                try{
                    ExecuteStages.deployArtifact(environmentConfigurations, strategyConfigurations, this)

                }catch(Exception ex){
                    throw ex
                }
            }
        }
    }
}